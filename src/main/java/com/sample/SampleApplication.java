package com.sample;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class SampleApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		return Collections.singleton(Sample.class);
	}

}
