package com.sample;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/sample")
public class Sample {

	@GET
	public Response get() {
		return Response.ok(new Bean("foo", "bar")).build();
	}
	
}
