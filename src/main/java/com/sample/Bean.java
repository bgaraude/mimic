package com.sample;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Bean {

	@XmlElement
	String id;

	@XmlElement
	String value;

	public Bean() {
	}

	public Bean(String id, String value) {
		this.id = id;
		this.value = value;
	}

}
